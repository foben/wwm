Rails.application.routes.draw do
  root to: "application#index"

  get 'gameplay/new', as: 'new_game'
  post 'gameplay/parse_json', as: 'parse_json'
  get 'gameplay/play', as: 'play'
  get 'gameplay/submit_answer/:answer' => 'gameplay#submit_answer', as: 'submit_answer'
  get 'gameplay/game_over/:gameover_session' => 'gameplay#game_over', as: 'game_over'
  get 'gameplay/bank_money' => 'gameplay#bank_money', as: 'bank_money'

  get 'games' => 'games#index', as: 'highscores'
  get 'games/:id/questions' => 'games#game_questions', as: 'game_questions'

  get 'joker/fifty/:game_id' => 'joker#fifty_fifty', as: 'fifty_fifty'
  get 'joker/audience/:game_id' => 'joker#audience', as: 'audience'

  get 'about' => 'about#index', as: 'about'
end
