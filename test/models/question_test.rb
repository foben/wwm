require 'test_helper'

class QuestionTest < ActiveSupport::TestCase

  test "should not save question without values and associated game" do
    q = Question.new
    assert_not q.save, "Saved question without any input"
    q.level = 2
    assert_not q.save, "Saved question only with level"
    q.text = "My question"
    assert_not q.save, "Saved question only with level, text"
    q.answer0 = "A0"
    q.answer1 = "A1"
    q.answer2 = "A2"
    q.answer3 = "A3"
    assert_not q.save, "Saved question only with level, text, answers"
    q.solution = 2
    assert_not q.save, "Saved question only with level, text, answers, solution"
    g = Game.new
    g.session_id = "DUMMY"
    q.game = g
    assert_not q.save, "Saved question with unsaved game"
    g = Game.new
    g.session_id = "DUMMY"
    g.save
    q.game = g
    assert q.save, "Should have worked now"

  end

end
