require 'test_helper'

class GameTest < ActiveSupport::TestCase

  test "should create default values for games" do
    game = Game.new
    game.session_id = "DUMMY"
    assert game.save
    assert game.current_level == 0, "Did not have default for current_level"
    assert game.cash_safe == 0, "Did not have default for cash_safe"
    assert game.cash_won == 0, "Did not have default for cash_won"
    assert game.cash_current == 0, "Did not have default for cash_current"
    assert game.running , "Game was not set running"
    assert_not game.fifty_used , "Fifty was not set unused"
  end

end
