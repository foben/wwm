class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :level
      t.text :text
      t.text :answer0
      t.text :answer1
      t.text :answer2
      t.text :answer3
      t.integer :solution
      t.belongs_to :game

      t.timestamps
    end
  end
end
