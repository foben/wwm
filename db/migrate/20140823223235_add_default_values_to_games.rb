class AddDefaultValuesToGames < ActiveRecord::Migration
  def up
    change_column :games, :current_level, :integer, :default => 0
    change_column :games, :cash_current, :integer, :default => 0
    change_column :games, :cash_won, :integer, :default => 0
    change_column :games, :cash_safe, :integer, :default => 0
    change_column :games, :running, :boolean, :default => true
  end

  def down
    change_column :games, :current_level, :integer, :default => nil
    change_column :games, :cash_current, :integer, :default => nil
    change_column :games, :cash_won, :integer, :default => nil
    change_column :games, :cash_safe, :integer, :default => nil
    change_column :games, :running, :boolean, :default => nil
  end
end
