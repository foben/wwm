class AddAudienceToGames < ActiveRecord::Migration
  def change
    add_column :games, :audience_used, :boolean, :default => false
  end
end
