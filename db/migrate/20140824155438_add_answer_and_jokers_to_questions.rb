class AddAnswerAndJokersToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :answered, :integer, :default => -1
  end
end
