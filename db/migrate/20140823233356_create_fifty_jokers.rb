class CreateFiftyJokers < ActiveRecord::Migration
  def change
    create_table :fifty_jokers do |t|
      t.integer :wrong1
      t.integer :wrong2
      t.belongs_to :question
      t.timestamps
    end
  end
end
