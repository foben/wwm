class AddWinningsToGames < ActiveRecord::Migration
  def change
    add_column :games, :cash_current, :integer
    add_column :games, :cash_safe, :integer
    add_column :games, :cash_won, :integer
  end
end
