class CreateAudienceJokers < ActiveRecord::Migration
  def change
    create_table :audience_jokers do |t|
      t.integer :perc0
      t.integer :perc1
      t.integer :perc2
      t.integer :perc3
      t.belongs_to :question
      t.timestamps
    end
  end
end
