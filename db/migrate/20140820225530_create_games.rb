class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :session_id
      t.integer :current_level
      t.boolean :running

      t.timestamps
    end
  end
end
