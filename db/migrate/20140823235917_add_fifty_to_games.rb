class AddFiftyToGames < ActiveRecord::Migration
  def change
    add_column :games, :fifty_used, :boolean, :default => false
  end
end
