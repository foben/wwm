# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140824234009) do

  create_table "audience_jokers", force: true do |t|
    t.integer  "perc0"
    t.integer  "perc1"
    t.integer  "perc2"
    t.integer  "perc3"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fifty_jokers", force: true do |t|
    t.integer  "wrong1"
    t.integer  "wrong2"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "games", force: true do |t|
    t.string   "session_id"
    t.integer  "current_level", default: 0
    t.boolean  "running",       default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cash_current",  default: 0
    t.integer  "cash_safe",     default: 0
    t.integer  "cash_won",      default: 0
    t.boolean  "fifty_used",    default: false
    t.boolean  "audience_used", default: false
  end

  create_table "questions", force: true do |t|
    t.integer  "level"
    t.text     "text"
    t.text     "answer0"
    t.text     "answer1"
    t.text     "answer2"
    t.text     "answer3"
    t.integer  "solution"
    t.integer  "game_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "answered",   default: -1
  end

end
