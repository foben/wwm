class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def index
    @game = Game.where(session_id: session.id).where(running: true).order(created_at: :desc).first
  end
end
