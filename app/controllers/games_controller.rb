class GamesController < ApplicationController

  def index
    #Display highest scoring games first.
    @games = Game.all.where(running: false).order(cash_won: :desc)
  end

  def game_questions
    #Display all answered questions for a particular game.
    game_id = params[:id]
    @questions = Game.find(game_id).questions.where.not(answered: -1)
  end

end
