require 'open-uri'
require 'json'

class GameplayController < ApplicationController
  before_action :get_game_and_current_question,
    only: [:play, :submit_answer, :game_over, :bank_money]

  def get_game_and_current_question
    #Get the game associated with this session
    #and the question for the current level
    #
    @game = Game.where(session_id: session.id).where(running: true).order(created_at: :desc).first
    if @game.nil? || !@game.running
      render action: :no_game_running
    else
      @current_question = @game.questions.where(level: @game.current_level).first
      @answers = [@current_question.answer0, @current_question.answer1,
                  @current_question.answer2, @current_question.answer3]
    end
  end

  def new
    #Render the create game form.
  end

  def play
    #The main game view.
    #Set up variables that contain joker information (if any).
    @fifty_wrong = []
    if @current_question.fifty_joker
      @fifty_wrong << @current_question.fifty_joker.wrong1
      @fifty_wrong << @current_question.fifty_joker.wrong2
    end

    if @current_question.audience_joker
      aud = @current_question.audience_joker
      @audience_joker = [aud.perc0, aud.perc1, aud.perc2, aud.perc3]

    end
  end

  def game_over
    #This finishes the game.
    #Check session param to avoid accidental stopping of current game.
    gameover_session = params[:gameover_session]
    if gameover_session != session.id
      render plain: 'You probably don\'t want to do that...'
    end
    @game.update(running: false)
  end

  def bank_money
    #Game action: Take won money and finish.
    finish_game_right
  end

  def submit_answer
    #Game action: Submit answer to the current question.

    #Check that parameter is a valid integer.
    begin
      answer = Integer(params[:answer], 10)
    rescue ArgumentError
      render plain: 'Not a valid submission'
      return
    end

    @current_question.update(answered: answer)
    right_answer = @current_question.solution
    if right_answer == answer
      #Update current winnings and fallback-level.
      @game.cash_current = Game.winnings_table[@game.current_level][0]
      @game.cash_safe = Game.winnings_table[@game.current_level][1]
      if @game.current_level == 14
        flash[:success] = "Congratulations, Millionaire!"
        finish_game_right
      else
        #Advance to the next level.
        flash[:success] = "Well done, that was correct!"
        @game.current_level += 1
        @game.save
        redirect_to play_path
      end
    else
      flash[:error] = "Unfortunately, that was not correct!"
      finish_game_wrong
    end
  end

  def parse_json
    #A JSON-file url is posted to this action.
    #Questions from the file are grouped by level and
    #one question is selected from each level.
    #For each, a Question Model-object is created and saved to the DB.
    #Error handling is very basic, catching all errors in a single block

    #If an old, unfinished game for this session exists, delete it and
    #all corresponding questions
    old = Game.all.where(running: true).where(session_id: session.id).first
    if not old.nil?
      old.questions.destroy_all
      old.destroy
    end

    #Parse supplied (remote) JSON file.
    json_url = params[:json_url]

    #Create new game with current session id.
    #For the remaining fields, defaults are set
    g = Game.new
    g.session_id = session.id
    g.save

    #Begin 'critical' code block.
    #First, check if the file exists and is not too large (3MB)
    begin
      if not Game.check_file_validity(json_url)
        raise "The file is not valid"
      end

      json = JSON.parse(open(json_url).read)
      #Seperate questions by level.
      levels = Hash.new {|h,k| h[k] = [] }
      json['questions'].each do |q|
        levels[q['level']] << q  
      end

      #Select a random question for each level
      #and create a question from it
      levels.each do |level, list|
        list.shuffle!
        selected = list[0]
        q = Question.new
        q.level = selected['level']
        q.text = selected['text']
        q.answer0 = selected['answers'][0]
        q.answer1 = selected['answers'][1]
        q.answer2 = selected['answers'][2]
        q.answer3 = selected['answers'][3]
        q.solution = selected['solution']
        q.game = g
        q.save()
      end
    rescue
      flash[:error] = "The file does not exist, is too big or could not be parsed!"
      #Since an error occured, delete all questions that may have been
      #instantiated as well as the game object itself
      g.questions.destroy
      g.destroy
      redirect_to new_game_path
      return
    end

    redirect_to action: :play
  end

  private
  def finish_game_right
    #The game was finished either by banking money or winning a Million.
    @game.cash_won = @game.cash_current
    @game.save
    redirect_to game_over_path(session.id)
  end

  private
  def finish_game_wrong
    #Game was finished by answering wron.
    #Set winnings to last fallback level.
    @game.cash_won = @game.cash_safe
    @game.save
    redirect_to game_over_path(session.id)
  end

  private
  def no_game_running
    #Render no game view.
  end

end
