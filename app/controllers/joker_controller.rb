require 'set'

class JokerController < ApplicationController

  def fifty_fifty
    #Use Fifty/Fifty Joker
    #
    game = Game.find(params[:game_id])
    if game.fifty_used
      flash[:alert] = "You already used your 50/50 Joker!"
      redirect_to play_path
      return
    end
    current_level = game.current_level
    question = game.questions.where(level: current_level).first

    candidates = [0, 1, 2, 3]
    candidates.delete(question.solution)
    candidates.shuffle
    
    ff = FiftyJoker.new
    ff.wrong1 = candidates[0]
    ff.wrong2 = candidates[1]
    ff.question = question
    ff.save
    game.update(fifty_used: true)

    redirect_to play_path
  end

  def audience
    game = Game.find(params[:game_id])
    if game.audience_used
      flash[:alert] = "You already used your Audience Joker!"
      redirect_to play_path
      return
    end
    current_level = game.current_level
    question = game.questions.where(level: current_level).first

    #Calculate the audiences 'response'.
    #The higher the level of the question, the less likely the
    #majority of the audience knows the right answer.
    
    results = [0, 0, 0, 0]
    others = [0, 1, 2, 3]
    others.delete(question.solution)
    ff = question.fifty_joker
    if not ff.nil?
      others.delete(ff.wrong1)
      others.delete(ff.wrong2)
    end
    puts others
    puts question.solution
    100.times do
      if rand(100) < 80 - 4 * current_level
        results[question.solution] += 1
      else
        results[others[rand(others.length)]] += 1
      end
    end
    aj = AudienceJoker.new
    aj.perc0 = results[0]
    aj.perc1 = results[1]
    aj.perc2 = results[2]
    aj.perc3 = results[3]
    aj.question = question
    aj.save
    game.update(audience_used: true)

    redirect_to play_path

  end
end
