require 'net/http'

class Game < ActiveRecord::Base
  def self.winnings_table
    {0 => [50, 0],
     1 =>  [100, 0],
     2 =>  [200, 0],
     3 =>  [300, 0],
     4 =>  [500, 500],
     5 =>  [1000, 500],
     6 =>  [2000, 500],
     7 =>  [4000, 500],
     8 =>  [8000, 500],
     9 =>  [16000, 16000],
     10 =>  [32000, 16000],
     11 =>  [64000, 16000],
     12 =>  [125000, 16000],
     13 =>  [500000, 16000],
     14 =>  [1000000, 16000],
    }

  end

  validates :session_id, :current_level, presence: true
  validates :current_level, numericality: { only_integer: true}
  has_many :questions, dependent: :destroy

  def self.check_file_validity(address)
    begin
      url = URI.parse(address)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = (url.scheme == "https")
      response = http.request_head(url.path)
      size = Integer(response['content-length'])
      if size > 1024 * 1024 * 3
        return false
      end
    rescue URI::InvalidURIError, SocketError
      return false
    end
    return true
  end
end
