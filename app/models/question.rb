class Question < ActiveRecord::Base
  validates :level, :text, :answer0, :answer1, :answer2, :answer3, :solution, :game_id, presence: true
  validates :level, :solution, numericality: { only_integer: true}
  belongs_to :game
  has_one :fifty_joker
  has_one :audience_joker
  
end
